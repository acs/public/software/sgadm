import re
from typing import DefaultDict
from numpy.core.numeric import full
from pandas import read_excel
from openpyxl import load_workbook
import pandas
import numpy as np
import analysis 
import streamlit as sl
import streamlit.components.v1 as components



def importXl(path):
    # import asset details from xl
    wb = load_workbook(path, read_only=True)
    sheet_name_edge = "Dependencies"
    sheet_name_node = "Assets"
    asset_dict.clear()
    dep_dict.clear()
    df_nodes = pandas.DataFrame()
    df_edges = pandas.DataFrame()
    col_check_asset = False
    check_asset_name_unique = False
    col_check_dep = False
    sheet_tab_names = False
    all_asset_input = True
    all_edge_input = True
    if sheet_name_edge and sheet_name_node in wb.sheetnames:
        sheet_tab_names = True
        df_nodes = read_excel(path, sheet_name=sheet_name_node)
        df_edges = read_excel(path, sheet_name=sheet_name_edge)

        required_asset_columns = ['ID', 'Asset Name', 'Domains', 'Zones', 'Layers', 'Location', 'Vulnerability Score']
        required_dep_columns = ['ID', 'From Asset', 'To Asset', 'Dependency Type']
        # check if all required columns are present in Assets the excel tab
        col_check_asset = all(elem in list(df_nodes.columns)  for elem in required_asset_columns)
        
        check_asset_name_unique = pandas.Series(df_nodes["Asset Name"].dropna()).is_unique

        selected_columns_df_nodes = df_nodes[["ID", "Asset Name", "Domains", "Zones", "Layers", "Location", "Vulnerability Score"]]
        new_df_nodes = selected_columns_df_nodes.copy()

        selected_columns_df_edges = df_edges[["ID", "From Asset", "Dependency Type", "To Asset"]]
        new_df_edges = selected_columns_df_edges.copy()

        if col_check_asset and check_asset_name_unique:
            for index, row in selected_columns_df_nodes.iterrows():
                domains = row['Domains']
                zones = row['Zones']
                layers = row['Layers']
                location = row['Location']
                vulscore =  row['Vulnerability Score']
                asset_name = row['Asset Name'] 
                node_id = row['ID']
                # remove rows which have Nan in all columns

                if (pandas.isnull(node_id) or pandas.isna(node_id)) and (pandas.isnull(domains) or pandas.isna(domains)) and (pandas.isnull(zones) or pandas.isna(zones)) and (pandas.isnull(layers) or pandas.isna(layers)) and (pandas.isnull(location) or pandas.isna(location)) and (pandas.isnull(vulscore) or pandas.isna(vulscore)):
                    new_df_nodes.drop(labels=index, axis=0, inplace=True)
                else:
                    if pandas.isnull(domains) or pandas.isna(domains): all_asset_input = False
                    else: domains = row['Domains'].split(",")
                    
                    if pandas.isnull(zones) or pandas.isna(zones): all_asset_input = False
                    else: zones = row['Zones'].split(",")
                    
                    if pandas.isnull(layers) or pandas.isna(layers): all_asset_input = False
                    else: layers = row['Layers'].split(",")
                    
                    if pandas.isnull(location) or pandas.isna(location): all_asset_input = False
                    else: location = row['Location'].split(",") 
                    
                    if pandas.isnull(vulscore) or pandas.isna(vulscore): 
                        vulscore = 0
                        vul_select = False
                    
                    if pandas.isnull(node_id) or pandas.isna(node_id): all_asset_input = False
                    

                    if pandas.isnull(asset_name) or pandas.isna(asset_name): all_asset_input = False
                    
                    asset_dict[row['Asset Name']] = {'ID' : row['ID'], 'AssetName' : row['Asset Name'], 'Domains' : domains,
                                        'Zones' : zones,'Layers' : layers, 'Location' : location, 'VulScore' : vulscore}
            new_df_nodes = new_df_nodes.astype({"ID": int})
        # check if all required columns are present in Dependency excel tab
        col_check_dep = all(elem in list(df_edges.columns)  for elem in required_dep_columns)
        if col_check_dep:
            for index, row in df_edges.iterrows():
                edge_id = row['ID']
                from_asset = row['From Asset']
                to_asset = row['To Asset']
                dep_typ = row['Dependency Type']
                if (pandas.isnull(edge_id) or pandas.isna(edge_id)) and (pandas.isnull(from_asset) or pandas.isna(from_asset)) and (pandas.isnull(to_asset) or pandas.isna(to_asset)) and (pandas.isnull(dep_typ) or pandas.isna(dep_typ)):
                    new_df_edges.drop(labels=index, axis=0, inplace=True)
                else:
                    if pandas.isnull(edge_id) or pandas.isna(edge_id) or pandas.isnull(from_asset) or pandas.isna(from_asset) or pandas.isnull(to_asset) or pandas.isna(to_asset) or pandas.isnull(dep_typ) or pandas.isna(dep_typ): all_edge_input = False
                   
                    dep_dict[row['ID']] = {'ID' :  row['ID'], 'FromAsset' : row['From Asset'], 'ToAsset' : row['To Asset'],
                                    'DepType' : row['Dependency Type']}
    return new_df_nodes, new_df_edges, col_check_asset, check_asset_name_unique, col_check_dep, sheet_tab_names, all_asset_input, all_edge_input  


def onChangeFunction():
    sl.session_state.source_or_target_changed = True

def makeCompleteGraphExpander(nx_graphs_dict, graph_name_html, df_nodes, all_assets, cia_comp_weights):
    orig_g = analysis.createPyVisGraph(graph_dict['All'], True)
    orig_content = saveGraphHtml(orig_g, graph_name_html)   
    highlight_node = []
    highlight_edge = []
    cascading_assets = DefaultDict()
    cascading_edges = DefaultDict()
       
    # adding option for viewing each dependency edge separately
    graph_col1, graph_col2, graph_col3 = sl.columns(3)
    edge_operations = ["None", "Show each dependency", "Adjust edge length"]
    edge_op = graph_col1.selectbox('Select edge operations:', edge_operations)
    smooth_edge = False
    if edge_op == "Show each dependency":
        smooth_edge = True
    
    # adding selection to view a particular network
    which_network=['All', 'IM Components', 'PES Components', 'Information', 'Functional', 'Communication', 'Business', 'Human']
    network_to_display = graph_col2.selectbox('Select asset class to display:', which_network)
    selected_graph = nx_graphs_dict[network_to_display]
    
    # adding selection to highlight a particular asset -- helps to orient in huge graph
    all_assets = df_nodes['Asset Name'].drop_duplicates()
    all_assets.loc[-1] = 'None'  # adding a row
    all_assets.index = all_assets.index + 1  # shifting index
    all_assets = all_assets.sort_index()  # sorting by index
    highlight_asset = graph_col3.selectbox('Select asset to highlight:', all_assets)
    if highlight_asset != 'None':
        highlight_node.append(highlight_asset)


    sl.subheader("Threat Paths")
    sl.markdown("""*Select source and target assets to view all possible propagation paths*""")
    threat_col1, threat_col2 = sl.columns(2)
    source_asset = threat_col1.selectbox('Select source asset:', all_assets, key = 'source_asset', on_change=lambda:onChangeFunction())
    target_asset = threat_col2.selectbox('Select target asset:', all_assets, key = 'target_asset', on_change=lambda:onChangeFunction())
    if 'source_asset' not in sl.session_state:
        sl.session_state.source_asset = source_asset
    if 'target_asset' not in sl.session_state:
        sl.session_state.target_asset = target_asset
    if 'source_or_target_changed' not in sl.session_state:
        sl.session_state.source_or_target_changed = False
    
    # from source to target asset, find all paths
    # adding selection to highlight a particular path
    if sl.session_state.source_or_target_changed:
        path_counts, paths, all_paths_nodes, all_paths_edges, path_details = analysis.findThreatPaths(source_asset, target_asset, nx_graphs_dict['All'], cia_comp_weights)    
            
        df_paths_counts = pandas.DataFrame.from_dict(path_counts)
        df_paths = pandas.DataFrame.from_dict(paths)
        sl.markdown("*All possible paths from asset *" + source_asset + " * to asset * " + target_asset)
        sl.dataframe(df_paths_counts.set_index('Path ID'))
        path_id = df_paths_counts['Path ID'].drop_duplicates()
        path_id.loc[-1] = 'None'  # adding a row
        path_id.index = path_id.index + 1  # shifting index
        path_id = path_id.sort_index()  # sorting by index
        paths_report = sl.button("Export paths details")
        if paths_report:
            filename = "paths_results.xlsx"
            try:
                df_paths.to_excel(filename)
            except OSError as err:
                if err.errno == 13:
                    pb = '''<p style="font-size:14px; color:red; font-weight:bold; font-style:italic;">
                            Please close paths_results.xlsx to save new paths result  </p>'''
                    sl.markdown(pb, unsafe_allow_html=True)
        
        path_to_highlight = sl.selectbox('Select path to highlight:', path_id)

        if path_to_highlight != 'None':
            for n in all_paths_nodes[path_to_highlight-1]: highlight_node.append(n)
            for e in all_paths_edges[path_to_highlight-1]: highlight_edge.append(e)
            smooth_edge = True
            
            sl.write('**Domains: **' + df_paths['Domains'].iloc[path_to_highlight-1] ) 
            sl.write('**Zones: **' + df_paths['Zones'].iloc[path_to_highlight-1] )
            sl.write('**Asset Nature/Layers: **' + df_paths['Layers'].iloc[path_to_highlight-1] )
            sl.write('**Locations: **' + df_paths['Locations'].iloc[path_to_highlight-1] )
            sl.write('**Asset Classes: **' + df_paths['Asset Classes'].iloc[path_to_highlight-1]) 
            #sl.write('**Vulnerability Classes: **' + df_paths['Vulnerability Classes'].iloc[path_to_highlight-1])

    # view failure/attack impact cascading. State of an asset (and also dependency) can be: Secure, Low-Critical, Medium-Critical and High-Critical
    sl.subheader("Cascading")
    sl.markdown("*View level wise fault/attack impact cascading and state of each asset during cascading based on it's dependencies*")
    cascade_col1, cascade_col2 = sl.columns(2)
    initial_impacted_asset = cascade_col1.selectbox('Select an asset:', all_assets)
    possible_states = ['None', 'Secure', 'Low-Critical', 'Medium-Critical', 'High-Critical']
    initial_impacted_asset_state = cascade_col2.selectbox('Select the state of asset:', possible_states)
    pb = '''<p style="font-size:14px; color:grey; font-weight:bold; font-style:italic;">
            Legend in graph: 
            <span style="color: #03DAC6">Secure</span>
            <span style="color: #FFA07A">Low-Critical</span>
            <span style="color: #FF4C00">Medium-Critical</span>
            <span style="color: #FF0000">High-Critical</span> 
            </p>'''

    sl.markdown(pb, unsafe_allow_html=True)
    if initial_impacted_asset != 'None' and (initial_impacted_asset_state == 'None' or initial_impacted_asset_state == 'Secure'):
        sl.write('*No further cascading possible*')
    elif initial_impacted_asset != 'None' and initial_impacted_asset_state != 'None':
        cascading_level_details = analysis.viewCascading(nx_graphs_dict['All'], initial_impacted_asset, initial_impacted_asset_state)       
        levels = list(cascading_level_details.keys())
        view_cascade_level = sl.selectbox('Select cascade level to view:', levels)
        if view_cascade_level != 'None':
            cascading_assets = cascading_level_details[view_cascade_level][0]
            cascading_edges = cascading_level_details[view_cascade_level][1]

    # create pyvis graph from networkx graph for the selected graph
    x = components.html(orig_content, height = 450, width = 1300)
    content_changed = False  
    updated_g = analysis.createPyVisGraph(selected_graph, True, highlightAsset=highlight_node, highlightEdge = highlight_edge, cascadingAssets=cascading_assets, cascadingEdges=cascading_edges )
    if smooth_edge:
        updated_g.set_edge_smooth('dynamic')
    
    if edge_op == "Adjust edge length":
        updated_g.toggle_physics(False)

    new_content = saveGraphHtml(updated_g, graph_name_html)  
    
    if new_content != orig_content:  
        content_changed = True
    if content_changed:
        x.empty()
        components.html(new_content, height = 450, width = 1300)
    

def saveGraphHtml(g, html_name):
    g.save_graph(html_name)

    #### For only local show no use of network , make sure to update .css & .js file path from network to local using python
    fh= open(html_name)
    content = fh.read()
    content = content.replace(re.findall("https://cdnjs.cloudflare.com/ajax/libs/vis/4.16.1/",content)[0],"")
    fh.close()
    fh=open(html_name,'w')
    fh.write(content)
    fh.close()
    return content 


if __name__ == '__main__':
    sl.title("Smart Grid Assets and Dependencies Model")
    sl.subheader("""*This tool displays smart grid assets information and dependencies using a graph-based representation*""")
    # asset_dict stores all assets with its attributes in dictionary form to be used later as nodes of a graph
    asset_dict = dict()
    # dep_dict stores all dependencies with its attributes in dictionary form to be used later as edges of a graph
    dep_dict = dict()

    #sl.sidebar.title('Create your smart grid asset view')
    sl.markdown("""*Upload excel file with assets and dependencies information*""")
    file_types = ['xlsx', 'xlsm', 'xls']
    uploaded_file = sl.file_uploader("Choose a file", type=file_types)  

    # These are cia weights per asset classes. These weights should be obtained using AHP with Questionnaire - order of list : C, I, A
    cia_comp_weights = {'PES Component' : [0.096, 0.284, 0.619], 'IM Component' : [0.627, 0.292, 0.081], 'Communication' : [0.702, 0.227, 0.072], 'Information' : [0.455, 0.455, 0.091],
                'Functional' : [0.085, 0.244, 0.671], 'Business' : [0.297, 0.539, 0.164], 'Human' : [0.444, 0.111, 0.444]}
    
    # These are are comparison matrix of the C,I,A, with criterias as asset classes
    # oder of asset class list: PES Component, IM Component, Communication, Information, Functional, Business, Human
    assetclass_comp_weight = {'Confidentiality' : [0.042, 0.273, 0.091, 0.334, 0.080, 0.120, 0.060], 'Integrity' : [0.157, 0.291, 0.054, 0.335, 0.072, 0.061, 0.030], 'Availability' : [0.375, 0.128, 0.072, 0.195, 0.151, 0.044, 0.035]}

    if uploaded_file is not None:
        df_nodes, df_edges, col_chk_asset, asset_name_unique, col_chk_dep, sheet_tab_names, all_asset_inputs, all_dep_inputs = importXl(uploaded_file)
        num_assets = len(df_nodes.index)
        #check if from and to assets are in assets lits
        if sheet_tab_names:
            all_assets_in_dep = list(dict.fromkeys(df_edges['From Asset'].tolist() + df_edges['To Asset'].tolist()))
            all_assets_in_dep_exists = all(elem in df_nodes['Asset Name'].tolist()  for elem in all_assets_in_dep)
            if col_chk_asset and col_chk_dep and asset_name_unique and num_assets and all_assets_in_dep_exists and all_asset_inputs and all_dep_inputs:
                all_assets = df_nodes['Asset Name'].drop_duplicates()
                all_assets.loc[-1] = 'None'  # adding a row
                all_assets.index = all_assets.index + 1  # shifting index
                all_assets = all_assets.sort_index()  # sorting by index
                
                asset_dep_expander = sl.expander(label='View Assets and Dependencies')
                asset_dep_expander.header('List of Assets and Dependencies')
                asset_dep_expander.subheader('Assets')
                asset_dep_expander.write('Total assets: ' + str(df_nodes.shape[0]) ) 
                asset_dep_expander.dataframe(df_nodes.set_index('ID'))

                asset_dep_expander.subheader('Dependencies')
                asset_dep_expander.dataframe(df_edges.set_index('ID'))
                
                domains_bar_chart = {'Generation'   :  df_nodes['Domains'].str.contains('Generation').sum(), 
                                    'Transmission' :  df_nodes['Domains'].str.contains('Transmission').sum(), 
                                    'Distribution' :  df_nodes['Domains'].str.contains('Distribution').sum(),
                                    'DER'          :  df_nodes['Domains'].str.contains('DER').sum(),  
                                    'Customer'     :  df_nodes['Domains'].str.contains('Customer').sum()}
                                    
                zones_bar_chart =   {'Process'      :  df_nodes['Zones'].str.contains('Process').sum(), 
                                    'Field'        :  df_nodes['Zones'].str.contains('Field').sum(), 
                                    'Station'      :  df_nodes['Zones'].str.contains('Station').sum(),
                                    'Operation'    :  df_nodes['Zones'].str.contains('Operation').sum(),  
                                    'Enterprise'   :  df_nodes['Zones'].str.contains('Enterprise').sum(),
                                    'Market'       :  df_nodes['Zones'].str.contains('Market').sum()}
            
                layers_bar_chart =  {'Component'    :  df_nodes['Layers'].str.contains('Component').sum(), 
                                    'Communication':  df_nodes['Layers'].str.contains('Communication').sum(), 
                                    'Information'  :  df_nodes['Layers'].str.contains('Information').sum(),
                                    'Functional'   :  df_nodes['Layers'].str.contains('Functional').sum(),  
                                    'Business'     :  df_nodes['Layers'].str.contains('Business').sum(),
                                    'Human'        :  df_nodes['Layers'].str.contains('Human').sum()}
                        
                asset_distribution = asset_dep_expander.checkbox(label='view asset distribution')
                if asset_distribution:
                    asset_dep_expander.markdown("**Asset Distribution Across Domains**")
                    asset_dep_expander.bar_chart( pandas.Series(domains_bar_chart, name='Number of assets').to_frame())
                    asset_dep_expander.markdown("**Asset Distribution Across Asset Nature/Layers**")
                    asset_dep_expander.bar_chart( pandas.Series(layers_bar_chart, name='Number of assets').to_frame())
                    asset_dep_expander.markdown("**Asset Distribution Across Zones**")
                    asset_dep_expander.bar_chart( pandas.Series(zones_bar_chart, name='Number of assets').to_frame())
                
                # Function to make graph
                all_graphs, it_components_graph, ot_components_graph, info_graph, human_graph, functional_graph, business_graph, communication_graph = analysis.createNetworkxGraphs(asset_dict,dep_dict)
                graph_dict = {'All' : all_graphs, 'IM Components' : it_components_graph, 'PES Components' : ot_components_graph, 'Information' : info_graph, 'Functional' : functional_graph, 'Communication' : communication_graph, 'Business' : business_graph, 'Human' : human_graph}
                
                # Expander to showcase assets impact indices
                asset_impact_expander = sl.expander(label='View Criticality Indices')
                asset_impact_expander.header("Criticality Indices")
                asset_class_critic = asset_impact_expander.checkbox(label='view asset class criticality')
                if asset_class_critic:
                    # asset class importance based on its ties to other classes and c,i,a priority index
                    asset_impact_expander.subheader("Asset Class Criticality")
                    asset_impact_expander.markdown("*Asset class criticality for each security objective based on number of out-connections of asset class, priority weight of security objective for given asset class and priority weight of asset class for given security objective*")
                    alpha = asset_impact_expander.slider("Tuning factor to vary importance of priority weights compared to asset class out degree", min_value=0.0, max_value=1.0, step=0.01, value=0.5)
                    ch_data1=analysis.findAssetClassImpIndex(alpha, graph_dict['All'], assetclass_comp_weight, cia_comp_weights)
                    asset_impact_expander.bar_chart(ch_data1) 
                # local impact is out degree centrality and global impact is closeness centrality
                gl_critic = asset_impact_expander.checkbox(label='view asset global versus local criticality')
                if gl_critic:
                    asset_impact_expander.subheader("Asset Global vs Local Criticality")
                    asset_impact_expander.markdown("*Global criticality of a node is an indication of number of cascading levels required to reach every other node in the network. This is found using the closeness centrality. The local criticality is an indication of proportion of assets which can be impacted during level-1 of cascading. This is found using the out degree centrality.*")
                    ch_data2 = analysis.findNodeLocalGlobalImpIndex(graph_dict['All'])
                    asset_impact_expander.bar_chart(ch_data2) 
                #cia_critic = asset_impact_expander.checkbox(label='view asset criticality for each security objective')
                #if cia_critic:
                #    # cia impact index
                #    asset_impact_expander.subheader("Asset Criticality for each security objective")
                #    asset_impact_expander.markdown("*Asset criticality based on impact of loss of an asset on each security objective*")
                #    ch_data3=analysis.findNodeCIAImpIndex(graph_dict['All'], assetclass_comp_weight)
                #    asset_impact_expander.bar_chart(ch_data3) 
                
                # expander for graph functions
                full_graph_expander = sl.expander(label='View Graph')
                full_graph_expander.header("Graph Representation")
                with full_graph_expander:
                    graph_name_html = "assets_dependencies" + ".html"   
                    makeCompleteGraphExpander(graph_dict, graph_name_html, df_nodes, all_assets, cia_comp_weights)
            if not col_chk_asset or not col_chk_dep:
                sl.write('*Not all required columns for Asset and Dependencies are available. Please check uploaded excel*')
                sl.write('Required columns for Assets are: ID, Asset Name, Domains, Zones, Layers, Location, Vulnerability Score')
                sl.write('Required columns for Dependencies are: ID, From Asset, To Asset, Dependency Type')
            if not asset_name_unique:
                sl.write('*Please enter unique asset names*')
            if not num_assets:
                sl.write('*No assets found*')
            if not all_assets_in_dep_exists:
                sl.write('*One or more assets in dependency list not found in asset list*')
            if not all_asset_inputs:
                sl.write('*One or more information related to assets are missing. Please check the Assets tab of input excel sheet and ensure that all asset information are provided*')
            if not all_dep_inputs:
                sl.write('*One or more information related to asset dependencies are missing. Please check the Dependencies tab of input excel sheet and ensure that all assets dependencies information are provided*')
            
        if not sheet_tab_names:
            sl.write('*Incorrect input-excel tab names. Tab names should be "Assets" and "Dependencies"*')

    

