import uvicorn
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
import uuid
from typing import List, Optional
from pydantic.main import BaseModel
from pydantic import Field



class Asset(BaseModel):
    id:Optional[uuid.UUID] = Field(uuid.uuid4())
    name:Optional[str] = None
    vulnerability:Optional[float] = None
    economic_value:Optional[float] = None


class Connection(BaseModel):
    id:Optional[uuid.UUID] = Field(uuid.uuid4())
    source:Optional[uuid.UUID] = None
    target:Optional[uuid.UUID] = None
    connection_type:Optional[str] = None

class AssetGraph(BaseModel):
    id:Optional[uuid.UUID] = Field(uuid.uuid4())
    assets:List[Asset] = []
    connections:List[Connection] = []

app = FastAPI()

app.add_middleware(CORSMiddleware,
                   allow_credentials=True,
                   allow_origins=["*"],
                   allow_methods=["*"],
                   allow_headers=["*"],
                   )


@app.get("/", response_model=AssetGraph)
def asset_graph() -> AssetGraph:
    asset1 = Asset()
    asset2 = Asset()

    asset1.name = "Asset1"
    asset1.vulnerability = 9.8
    asset1.economic_value = 5000

    asset2.name = "Asset2"
    asset2.vulnerability = 4.5
    asset2.economic_value = 2000

    connection1  = Connection()
    connection1.connection_type = "CONNECTS"
    connection1.source = asset1.id
    connection1.target = asset2.id

    assetgraph = AssetGraph()
    assetgraph.assets = [asset1, asset2]
    assetgraph.connections = [connection1]

    return assetgraph
    

if __name__ == '__main__':
    uvicorn.run(app, host="0.0.0.0", port=8000)
    