from typing import DefaultDict, Dict
import networkx as nx
from pyvis import network as net
import streamlit.components.v1 as components
import streamlit as sl
import pandas as pd
import numpy as np
import copy
from collections import defaultdict

def createNetworkxGraphs(asset_dict, dep_dict):
    it_components_graph = nx.MultiDiGraph()
    ot_components_graph = nx.MultiDiGraph()
    info_graph = nx.MultiDiGraph()
    human_graph = nx.MultiDiGraph()
    functional_graph = nx.MultiDiGraph()
    business_graph = nx.MultiDiGraph()
    communication_graph = nx.MultiDiGraph()
    all_graphs = nx.MultiDiGraph()
    
    for asset, attr in asset_dict.items():
        if 'Component' in attr['Layers'] and  'Process' not in attr['Zones']:
            it_components_graph.add_node(asset, name = attr['AssetName'], domains = attr['Domains'], layers = attr['Layers'], zones = attr['Zones'],
                                         location = attr['Location'], s = 'o', net='IM Component', state='Secure', vulscore = attr['VulScore'])   
            all_graphs.add_node(asset, name = attr['AssetName'], domains = attr['Domains'], layers = attr['Layers'], zones = attr['Zones'],
                                         location = attr['Location'], s = 'o', net='IM Component', state='Secure', vulscore = attr['VulScore'])     
        if  'Component' in attr['Layers'] and 'Process' in attr['Zones']:
            ot_components_graph.add_node(asset, name = attr['AssetName'], domains = attr['Domains'], layers = attr['Layers'], zones = attr['Zones'],
                                         location = attr['Location'], s='d', net='PES Component', state='Secure', vulscore = attr['VulScore'])   
            all_graphs.add_node(asset, name = attr['AssetName'], domains = attr['Domains'], layers = attr['Layers'], zones = attr['Zones'],
                                         location = attr['Location'], s = 'd', net='PES Component', state='Secure', vulscore = attr['VulScore'])   
        if 'Information' in attr['Layers']:
            info_graph.add_node(asset, name = attr['AssetName'], domains = attr['Domains'], layers = attr['Layers'], zones = attr['Zones'],
                                         location = attr['Location'], s='v', net='Information', state='Secure', vulscore = attr['VulScore']) 
            all_graphs.add_node(asset, name = attr['AssetName'], domains = attr['Domains'], layers = attr['Layers'], zones = attr['Zones'],
                                         location = attr['Location'], s = 'v', net='Information', state='Secure', vulscore = attr['VulScore'])     
        if 'Human' in attr['Layers']:
            human_graph.add_node(asset, name = attr['AssetName'], domains = attr['Domains'], layers = attr['Layers'], zones = attr['Zones'],
                                         location = attr['Location'], s='p', net='Human', state='Secure', vulscore = attr['VulScore']) 
            all_graphs.add_node(asset, name = attr['AssetName'], domains = attr['Domains'], layers = attr['Layers'], zones = attr['Zones'],
                                         location = attr['Location'], s = 'p', net='Human', state='Secure', vulscore = attr['VulScore'])    
        if 'Functional' in attr['Layers']:
            functional_graph.add_node(asset, name = attr['AssetName'], domains = attr['Domains'], layers = attr['Layers'], zones = attr['Zones'],
                                        location = attr['Location'], s='h', net='Functional', state='Secure', vulscore = attr['VulScore'])  
            all_graphs.add_node(asset, name = attr['AssetName'], domains = attr['Domains'], layers = attr['Layers'], zones = attr['Zones'],
                                         location = attr['Location'], s = 'h', net='Functional', state='Secure', vulscore = attr['VulScore'])   
        if 'Business' in attr['Layers']:
            business_graph.add_node(asset, name = attr['AssetName'], domains = attr['Domains'], layers = attr['Layers'], zones = attr['Zones'],
                                        location = attr['Location'], s='s', net='Business', state='Secure', vulscore = attr['VulScore'])
            all_graphs.add_node(asset, name = attr['AssetName'], domains = attr['Domains'], layers = attr['Layers'], zones = attr['Zones'],
                                         location = attr['Location'], s = 's', net='Business', state='Secure', vulscore = attr['VulScore'])    
        if 'Communication' in attr['Layers']:
            communication_graph.add_node(asset, name = attr['AssetName'], domains = attr['Domains'], layers = attr['Layers'], zones = attr['Zones'],
                                         location = attr['Location'], s='>', net='Communication', state='Secure', vulscore = attr['VulScore'])
            all_graphs.add_node(asset, name = attr['AssetName'], domains = attr['Domains'], layers = attr['Layers'], zones = attr['Zones'],
                                         location = attr['Location'], s = '>', net='Communication', state='Secure', vulscore = attr['VulScore'])    
    
    # adding relevant edges
    for edge_id, edge_attr in dep_dict.items():
        if "Connects" in edge_attr["DepType"]:
            all_graphs.add_edge(edge_attr["FromAsset"], edge_attr["ToAsset"], DepType=edge_attr["DepType"], state='Secure') 
            all_graphs.add_edge(edge_attr["ToAsset"], edge_attr["FromAsset"], DepType=edge_attr["DepType"], state='Secure') 
            if edge_attr["FromAsset"] in it_components_graph.nodes() and edge_attr["ToAsset"] in it_components_graph.nodes():
                it_components_graph.add_edge(edge_attr["FromAsset"], edge_attr["ToAsset"], DepType=edge_attr["DepType"], state='Secure')
                it_components_graph.add_edge(edge_attr["ToAsset"], edge_attr["FromAsset"], DepType=edge_attr["DepType"], state='Secure') 
            if edge_attr["FromAsset"] in ot_components_graph.nodes() and edge_attr["ToAsset"] in ot_components_graph.nodes():
                ot_components_graph.add_edge(edge_attr["FromAsset"], edge_attr["ToAsset"], DepType=edge_attr["DepType"], state='Secure')
                ot_components_graph.add_edge(edge_attr["ToAsset"], edge_attr["FromAsset"], DepType=edge_attr["DepType"], state='Secure') 
            if edge_attr["FromAsset"] in info_graph.nodes() and edge_attr["ToAsset"] in info_graph.nodes():
                info_graph.add_edge(edge_attr["FromAsset"], edge_attr["ToAsset"], DepType=edge_attr["DepType"], state='Secure')
                info_graph.add_edge(edge_attr["ToAsset"], edge_attr["FromAsset"], DepType=edge_attr["DepType"], state='Secure') 
            if edge_attr["FromAsset"] in human_graph.nodes() and edge_attr["ToAsset"] in human_graph.nodes():
                human_graph.add_edge(edge_attr["FromAsset"], edge_attr["ToAsset"], DepType=edge_attr["DepType"], state='Secure')
                human_graph.add_edge(edge_attr["ToAsset"], edge_attr["FromAsset"], DepType=edge_attr["DepType"], state='Secure') 
            if edge_attr["FromAsset"] in functional_graph.nodes() and edge_attr["ToAsset"] in functional_graph.nodes():
                functional_graph.add_edge(edge_attr["FromAsset"], edge_attr["ToAsset"], DepType=edge_attr["DepType"], state='Secure')
                functional_graph.add_edge(edge_attr["ToAsset"], edge_attr["FromAsset"], DepType=edge_attr["DepType"], state='Secure') 
            if edge_attr["FromAsset"] in business_graph.nodes() and edge_attr["ToAsset"] in business_graph.nodes():
                business_graph.add_edge(edge_attr["FromAsset"], edge_attr["ToAsset"], DepType=edge_attr["DepType"], state='Secure')
                business_graph.add_edge(edge_attr["ToAsset"], edge_attr["FromAsset"], DepType=edge_attr["DepType"], state='Secure') 
            if edge_attr["FromAsset"] in communication_graph.nodes() and edge_attr["ToAsset"] in communication_graph.nodes():
                communication_graph.add_edge(edge_attr["FromAsset"], edge_attr["ToAsset"], DepType=edge_attr["DepType"], state='Secure')
                communication_graph.add_edge(edge_attr["ToAsset"], edge_attr["FromAsset"], DepType=edge_attr["DepType"], state='Secure') 
        else:
            all_graphs.add_edge(edge_attr["FromAsset"], edge_attr["ToAsset"], DepType=edge_attr["DepType"], state='Secure') 

    return all_graphs, it_components_graph, ot_components_graph, info_graph, human_graph, functional_graph, business_graph, communication_graph

   
def createPyVisGraph(graph, direction, highlightAsset=[], highlightEdge = [], cascadingAssets={}, cascadingEdges={}):
    g=net.Network(height='400px', width='50%',heading='', directed=direction)
    
    shapes = {'PES Component' : 'circularImage', 'IM Component' : 'circularImage', 'Information' : 'diamond', 'Human' : 'star', 'Business' : 'triangleDown', 'Communication': 'square' , 'Functional' : 'triangle'}
    states_colours = {'Low-Critical' : '#FFA07A', 'Medium-Critical' : '#FF4C00', 'High-Critical' : '#FF0000'}
    for n in graph.nodes(data=True):
        if n[1]['name'] in cascadingAssets.keys():
            vul_cas_str = 'CL: ' + str(cascadingAssets[str(n[0])]['cascading level']) + '; VS: ' + str(n[1]['vulscore'])
            g.add_node(n[0], title= vul_cas_str, label=n[1]['name'], shape=shapes[n[1]['net']], image = '', color=states_colours[cascadingAssets[str(n[0])]['state']])
        elif n[1]['name'] in highlightAsset or n[0] in highlightAsset:
            vul_str = 'VS: ' + str(n[1]['vulscore'])
            # for "select asset to highlight" option on tool
            if len(highlightAsset) == 1:
                g.add_node(n[0], title= vul_str, label=n[1]['name'], shape=shapes[n[1]['net']], image = '', color='#FFA500')
            else:
                # to highlight threat paths, separate colour for source and target asset
                if n[1]['name'] == highlightAsset[0] or n[1]['name'] == highlightAsset[-1]:
                    g.add_node(n[0], title= vul_str, label=n[1]['name'], shape=shapes[n[1]['net']], image = '', color='#B59181')
                else:
                    g.add_node(n[0], title= vul_str, label=n[1]['name'], shape=shapes[n[1]['net']], image = '', color='#FFA500')
        else:
            vul_str = 'VS: ' + str(n[1]['vulscore'])
            g.add_node(n[0], title= vul_str, label=n[1]['name'], shape=shapes[n[1]['net']], image = '', color='#03DAC6')
    for e in graph.edges(data=True, keys=True):
        if (e[0], e[1], e[2]) in cascadingEdges.keys():
            g.add_edge(e[0], e[1], title=e[3]['DepType'], color=states_colours[cascadingEdges[(e[0], e[1], e[2])]['state']])
        elif (e[0], e[1], e[2], e[3]['DepType']) in highlightEdge:
            g.add_edge(e[0], e[1], title=e[3]['DepType'], color='#FFA500')
        else:
            g.add_edge(e[0], e[1], title=e[3]['DepType'], color='#03DAC6')
        
    g.inherit_edge_colors(False)
      
    return g


def findPathsFromSourceToTarget(source_asset, target_asset, graph, scanned, path_nodes, all_paths_nodes, path_edges, all_paths_edges):
    scanned[source_asset] = True
    path_nodes.append(source_asset)
    # if source and target asset are same, then exit and print the paths
    if source_asset == target_asset:
        all_paths_nodes.append(path_nodes.copy())
        all_paths_edges.append(path_edges.copy())
        # restricting number of paths to 200. This is done only to avoid the program from taking forever to find all paths. 
        if len(all_paths_edges) == 200:
            return
    else:
        #loop over all adjacent nodes of the source asset to find path to target_asset
        for e in graph.edges(data=True, keys=True): 
            #e[0] is the source_asset e[1] is the adjacent node
            if e[0] == source_asset:
                if scanned[e[1]] == False:
                    path_edges.append((e[0],e[1], e[2], e[3]['DepType']))
                    findPathsFromSourceToTarget(e[1], target_asset, graph, scanned, path_nodes, all_paths_nodes, path_edges, all_paths_edges)
    
    # restricting number of paths to 200. This is done only to avoid the program from taking forever to find all paths. 
    if len(all_paths_edges) == 200:
        return
    path_nodes.pop()
    path_edges.pop()
    scanned[source_asset] = False
        

def findThreatPaths(source_name, target_name, nx_graph, cia_weights):        
    path_details = DefaultDict()
    all_paths_nodes=[]
    all_paths_edges=[]
    for_plain_output_display = {'Path ID' : [], 'Domains' : [], 'Layers' : [], 'Zones' : [], 'Locations' : [], 'Total Assets' : [], 'CIA Order' : [], 'Group Out Degree' : [], 'Vulnerability Score' : [], 'Asset Classes' : [], 'Asset Names' : []}
    for_dataframe_counts = {'Path ID' : [], 'Domains' : [], 'Layers' : [], 'Zones' : [], 'Locations' : [], 'Total Assets' : [], 'CIA Order' : [], 'Group Out Degree' : [], 'Vulnerability Score' : []}
    
    # map of asset class to vulnerability classes
    asset_vul_map = {'PES Component' : 'Physical', 'IM Component' : 'Physical', 'Communication' : 'Cyber', 'Information' : 'Cyber', 'Functional' : 'Cyber', 'Business': 'Human/Organizational', 'Human' : 'Human/Organizational'}
   
    if source_name != 'None' and target_name != 'None':
        path_edges=['dummy']
        path_nodes=[]
        scanned=DefaultDict()
        for n in nx_graph.nodes():
            scanned[n] = False
        #findPathsFromSourceToTarget(source[0], target[0], nx_graph, scanned, path_nodes, all_paths_nodes, path_edges, all_paths_edges)
        findPathsFromSourceToTarget(source_name, target_name, nx_graph, scanned, path_nodes, all_paths_nodes, path_edges, all_paths_edges)

        index = 1
        all_out_deg = nx.out_degree_centrality(nx_graph)
        
        for paths in all_paths_nodes:
            d=[]
            l=[]
            z=[]
            a=[]
            asset_names=[]

            l_pescomp = 0
            l_imcomp = 0
            l_comm = 0
            l_info = 0
            l_func = 0
            l_bus = 0
            l_hum = 0
            vul_score = 0

            no_assets=len(paths)
            out_degree = 0
            for node in paths:
                out_degree = out_degree + all_out_deg[node]
                vul_score = vul_score + nx_graph.nodes()[node]['vulscore']
                asset_names.append(node)
                for each_domain in nx_graph.nodes()[node]['domains']:
                    d.append(each_domain)
                for each_layer in nx_graph.nodes()[node]['layers']:
                    l.append(each_layer)
                for each_zone in nx_graph.nodes()[node]['zones']:
                    z.append(each_zone)
                for each_area in nx_graph.nodes()[node]['location']:
                    if each_area != "None": a.append(each_area)
                if nx_graph.nodes()[node]['net'] == 'PES Component':
                    l_pescomp = l_pescomp + 1
                if nx_graph.nodes()[node]['net'] == 'IM Component':
                    l_imcomp = l_imcomp + 1
                if  nx_graph.nodes()[node]['net'] == 'Information' :
                    l_info = l_info + 1
                if  nx_graph.nodes()[node]['net'] == 'Functional':
                    l_func = l_func + 1
                if  nx_graph.nodes()[node]['net'] == 'Business':
                    l_bus = l_bus + 1
                if  nx_graph.nodes()[node]['net'] == 'Communication':
                    l_comm = l_comm + 1
                if  nx_graph.nodes()[node]['net'] == 'Human':
                    l_hum = l_hum + 1
            d = list(dict.fromkeys(d))
            l = list(dict.fromkeys(l))
            z = list(dict.fromkeys(z))
            a = list(dict.fromkeys(a))
                
            
            path_details[index] = {'Domains' : ','.join(d), 'Layers' : ','.join(l), 'Zones' : ','.join(z), 'Locations' : ','.join(a), 'PathLength' : no_assets, 'Group Out Degree' : nx.group_out_degree_centrality(nx_graph, paths),
                                    'PES Component' : l_pescomp, 'IM Component' : l_imcomp, 'Communication' : l_comm, 'Information' : l_info, 'Functional' : l_func, 'Business' : l_bus, 'Human' : l_hum, 'VulScore' : vul_score, 'Asset Names': asset_names}
            index = index+1      
        
        for id, val in path_details.items():
            # find CIA order for each path and find which asset classes are each path
            cia_order = {'C' : 0, 'I' : 0, 'A' : 0}
            path_asset_class = []
            for asset_class in cia_weights.keys():
                cia_order['C'] = cia_order['C'] + cia_weights[asset_class][0]*(val[asset_class]/val['PathLength'])
                cia_order['I'] = cia_order['I'] + cia_weights[asset_class][1]*(val[asset_class]/val['PathLength'])
                cia_order['A'] = cia_order['A'] + cia_weights[asset_class][2]*(val[asset_class]/val['PathLength'])
                # find asset classes for which assets are in given path
                if val[asset_class]:
                    path_asset_class.append(asset_class)
            path_vul_class = [val for key,val in asset_vul_map.items() if key in path_asset_class]
            path_vul_class = list(dict.fromkeys(path_vul_class))
            cia = ', '.join(dict(sorted(cia_order.items(), key=lambda item: item[1], reverse=True)))

            for_plain_output_display['Path ID'].append(id)
            for_plain_output_display['Domains'].append(val['Domains'])
            for_plain_output_display['Layers'].append(val['Layers'])
            for_plain_output_display['Zones'].append(val['Zones'])
            for_plain_output_display['Locations'].append(val['Locations'])
            for_plain_output_display['Asset Classes'].append(",".join(path_asset_class))
            for_plain_output_display['Total Assets'].append(str(val['PathLength']))
            for_plain_output_display['CIA Order'].append(cia)
            for_plain_output_display['Group Out Degree'].append(val['Group Out Degree'])
            for_plain_output_display['Vulnerability Score'].append(val['VulScore'])
            #for_plain_output_display['Vulnerability Classes'].append(",".join(path_vul_class))
            for_plain_output_display['Asset Names'].append(",".join(val['Asset Names']))

            for_dataframe_counts['Path ID'].append(id)
            for_dataframe_counts['Domains'].append(len(val['Domains'].split(',')))
            for_dataframe_counts['Layers'].append(len(val['Layers'].split(',')))
            for_dataframe_counts['Zones'].append(len(val['Zones'].split(',')))
            for_dataframe_counts['Locations'].append(len(val['Locations'].split(',')))
            for_dataframe_counts['Total Assets'].append(str(val['PathLength']))
            for_dataframe_counts['CIA Order'].append(cia)
            for_dataframe_counts['Group Out Degree'].append(val['Group Out Degree'])
            for_dataframe_counts['Vulnerability Score'].append(val['VulScore'])
            
        # remove the 'dummy' entry from paths
        if len(all_paths_edges):
            for each in all_paths_edges:
                each.pop(0)

    return for_dataframe_counts, for_plain_output_display, all_paths_nodes, all_paths_edges, path_details


def findNodeCIAImpIndex(nx_graph, prio_weights):
    # prio_weights is a dictionary with key Confidentiality, Availability, Integrity, and values are
    # list of priorities with order: [PES Component, IM Component, Communication, Information, Functional, Business, Human]
    
    c_prio = prio_weights['Confidentiality']
    a_prio = prio_weights['Availability']
    i_prio = prio_weights['Integrity']

    node_attr_net = nx.get_node_attributes(nx_graph,'net')
    pes_nodes = [key for key,val in node_attr_net.items() if val=='PES Component']
    im_nodes = [key for key,val in node_attr_net.items() if val=='IM Component']
    info_nodes = [key for key,val in node_attr_net.items() if val=='Information']
    comm_nodes = [key for key,val in node_attr_net.items() if val=='Communication']
    func_nodes = [key for key,val in node_attr_net.items() if val=='Functional']
    bus_nodes = [key for key,val in node_attr_net.items() if val=='Business']
    hum_nodes = [key for key,val in node_attr_net.items() if val=='Human']

    c_part1 = (len(pes_nodes)*c_prio[0] + len(im_nodes)*c_prio[1] + len(comm_nodes)*c_prio[2] + len(info_nodes)*c_prio[3] + len(func_nodes)*c_prio[4] + len(bus_nodes)*c_prio[5] + len(hum_nodes)*c_prio[6])
    i_part1 = (len(pes_nodes)*i_prio[0] + len(im_nodes)*i_prio[1] + len(comm_nodes)*i_prio[2] + len(info_nodes)*i_prio[3] + len(func_nodes)*i_prio[4] + len(bus_nodes)*i_prio[5] + len(hum_nodes)*i_prio[6])
    a_part1 = (len(pes_nodes)*a_prio[0] + len(im_nodes)*a_prio[1] + len(comm_nodes)*a_prio[2] + len(info_nodes)*a_prio[3] + len(func_nodes)*a_prio[4] + len(bus_nodes)*a_prio[5] + len(hum_nodes)*a_prio[6])     

    cols=['Confidentiality', 'Integrity', 'Availability']
    cia = []
    indexes=[]
    for n in nx_graph.nodes():
        cia_node=[]
        pes = 0 
        im = 0
        info = 0
        comm = 0
        func = 0
        bus = 0
        human = 0
        neighbors = list(nx_graph.neighbors(n))
        neighbors.append(n)
        indexes.append(n)
        for neighbor in neighbors:
            if nx_graph.nodes()[neighbor]['net'] == 'PES Component':
                pes = pes+1
            if nx_graph.nodes()[neighbor]['net'] == 'IM Component':
                im = im+1
            if nx_graph.nodes()[neighbor]['net'] == 'Information':
                info = info+1
            if nx_graph.nodes()[neighbor]['net'] == 'Communication':
                comm = comm+1
            if nx_graph.nodes()[neighbor]['net'] == 'Functional':
                func = func+1
            if nx_graph.nodes()[neighbor]['net'] == 'Business':
                bus = bus+1
            if nx_graph.nodes()[neighbor]['net'] == 'Human':
                human = human+1
        
        
        c_part2 = (pes*c_prio[0] + im*c_prio[1] + comm*c_prio[2] + info*c_prio[3] + func*c_prio[4] + bus*c_prio[5] + human*c_prio[6])
        cia_node.append(c_part2/c_part1)
        
        i_part2 = (pes*i_prio[0] + im*i_prio[1] + comm*i_prio[2] + info*i_prio[3] + func*i_prio[4] + bus*i_prio[5] + human*i_prio[6])
        cia_node.append(i_part2/i_part1)
   
        a_part2 = (pes*a_prio[0] + im*a_prio[1] + comm*a_prio[2] + info*a_prio[3] + func*a_prio[4] + bus*a_prio[5] + human*a_prio[6])
        cia_node.append(a_part2/a_part1)
                   
        cia.append(cia_node)
    
    chart_data= pd.DataFrame(cia, columns=cols, index=indexes)
    return chart_data


def findNodeLocalGlobalImpIndex(nx_graph):
    out_degree_centrality = nx.out_degree_centrality(nx_graph)
    closeness_centrality = nx.closeness_centrality(nx_graph.reverse()) #G.reverse as closeness with respect to out-going edges are considered

    impact_index=[]
    for n, deg in out_degree_centrality.items():
        loc_glo = []
        loc_glo.append(deg)
        if n in closeness_centrality.keys(): loc_glo.append(closeness_centrality[n])
        impact_index.append(loc_glo)

    chart_data= pd.DataFrame(impact_index, columns=['Local Impact', 'Global Impact'], index=list(out_degree_centrality.keys()))
    return chart_data


def findAssetClassImpIndex(alpha, nx_graph, assetclass_comp_weight, cia_comp_weights):

    asset_class_out_connections = {'PES Component' : 0, 'IM Component' : 0, 'Communication' : 0, 'Information' : 0, 'Functional' : 0, 'Business' : 0, 'Human' : 0}
    asset_class_weights = {'PES Component' : [], 'IM Component' : [], 'Communication' : [], 'Information' : [], 'Functional' : [], 'Business' : [], 'Human' : []}
    node_attr_net = nx.get_node_attributes(nx_graph,'net')
   
    for e in nx_graph.edges():
        from_asset = e[0]
        to_asset = e[1]
        from_net = nx_graph.nodes()[from_asset]['net'] 
        to_net = nx_graph.nodes()[to_asset]['net']
        if from_net != to_net:
            asset_class_out_connections[from_net] += 1 
    j = 0
    for ac, num_connects in asset_class_out_connections.items():
        total_num_nodes = [key for key,val in node_attr_net.items() if val==ac]
        c = alpha*len(total_num_nodes)*assetclass_comp_weight['Confidentiality'][j]*cia_comp_weights[ac][0] + (1-alpha)*num_connects
        i = alpha*len(total_num_nodes)*assetclass_comp_weight['Integrity'][j]*cia_comp_weights[ac][1] + (1-alpha)*num_connects
        a = alpha*len(total_num_nodes)*assetclass_comp_weight['Availability'][j]*cia_comp_weights[ac][2] + (1-alpha)*num_connects
        asset_class_weights[ac].append(c)
        asset_class_weights[ac].append(i)
        asset_class_weights[ac].append(a)
        j += 1 
       
    chart_data= pd.DataFrame(list(asset_class_weights.values()), columns=['Confidentiality', 'Integrity', 'Availability'], index=list(asset_class_weights.keys()))
    return chart_data

def viewCascading(nx_graph, initial_impact_asset, initial_impact_state):
    # step1 : copy the original network to track old states of assets and edges
    # step2: find dependent (outward) assets of the impacted asset
    # step4: record number of (in-)edges for each dependency category of the dependent asset found in step2
    # step5: assess the state of assets based on states of incoming edges and following rules:
        # rule 1 : atleast one (in-)edge from each dependency type is either Low-Critical, Medium-Critical or High-Critical, then mark asset as Medium-Critical --> This indicates that the asset is being impacted from all types of dependencies
        # rule 2 : if >0 , but <50% of (in-)edges from a given dependency type is either Low-Critical or Medium-Critical or High-Critical, then mark asset as Low-Critical
        # rule 3 : if >=50% (upto 100%) of (in-)edges of a given dependency type is Low-Critical or Medium-Critical, OR >=50%, but  <100% are High-Critical, then mark asset as Medium-Critical
        # rule 4 : if all (in-)edges of a given dependency type is High-Critical, then mark asset as High-Critical --> High-Critical can imply total unavailability of the asset, or all inputs required for asset to work properly are highly impacted
        # rule 5 : if none of the (in-)edges are in state "Secure", then mark asset state as High-Critical
    impacted_graph = copy.deepcopy(nx_graph)

    impacted_asset_in_queue = DefaultDict()      # this dictionary is for storing impacted assets and states in a cascading level
    impacted_asset_in_queue[initial_impact_asset] = initial_impact_state
    edge_update_state = DefaultDict()
    edge_update_state_connects = DefaultDict()   # this is used to set both directional edges of Connects to a given state
    next_impacted_assets=[initial_impact_asset]  # this list is additional for while loop condition and stores same assets as impacted_asset_in_queue
    cascading_level = 0
    all_impacted_assets=DefaultDict()            # this dictionary stores all assets, states and cascading levels for the complete cascading event
    all_impacted_edges = DefaultDict()
    all_impacted_assets[initial_impact_asset] = {'state' : initial_impact_state, 'cascading level' : cascading_level}
    # set the initial impacted asset state in the impacted_graph
    impacted_graph.nodes()[initial_impact_asset]['state'] = initial_impact_state
    cascading_level_details=defaultdict(list)

    while len(next_impacted_assets):
        # set states of outgoing edges of impact_asset to impact_state
        edge_update_state_connects.clear()
        edge_update_state.clear()
        
        # record assets and edges states per cascade level
        for e_from,e_to,key,attr in impacted_graph.edges(data=True, keys=True):
            if attr['state'] != 'Secure':
                all_impacted_edges[(e_from,e_to,key)] = {"state": attr['state']}

        this_cascade_assets = all_impacted_assets.copy()
        this_cascade_edges=all_impacted_edges.copy()
        cascading_level_details[cascading_level].append(this_cascade_assets)
        cascading_level_details[cascading_level].append(this_cascade_edges)
        
        for impacted_asset, impacted_asset_state in impacted_asset_in_queue.items():
            for e_from, e_to, key, attr in nx_graph.out_edges(impacted_asset, data=True, keys=True):
                if e_from == impacted_asset:
                    edge_update_state[(e_from, e_to, key)] = {"state": impacted_asset_state}
                    for dep_e_from, dep_e_to, dep_key, dep_attr in nx_graph.in_edges(e_to, data=True, keys=True):
                        if dep_e_from == e_from:
                            if dep_attr['DepType'] == attr['DepType']:
                                edge_update_state_connects[(dep_e_to, dep_e_from, dep_key)] = {"state": impacted_asset_state}
            nx.set_edge_attributes(impacted_graph, edge_update_state)
            # set other direction "Connects" and other bidirectional dependency type edges to the same state
            nx.set_edge_attributes(impacted_graph, edge_update_state_connects) 
        
        cascading_level += 1
        impacted_asset_in_queue.clear()
        next_impacted_assets=[]

        for edges, st in edge_update_state.items():
            atleaset_one_edge_secure = False
             
            # find number of number of smae type dependencies of the impacted asset and their states
            dep_of_impacted_assets = impacted_graph.in_edges(edges[1], data=True, keys=True)
            num_of_each_dep_of_impacted_asset = defaultdict(lambda: 0)
            num_of_each_dep_of_impacted_asset_st = defaultdict(list)
            for each_edge in dep_of_impacted_assets:
                num_of_each_dep_of_impacted_asset[each_edge[3]['DepType']] += 1
                # save only dep types whose states are not Secure
                if each_edge[3]['state'] != 'Secure':
                    num_of_each_dep_of_impacted_asset_st[each_edge[3]['DepType']].append(each_edge[3]['state'])
                elif each_edge[3]['state'] == 'Secure':
                    atleaset_one_edge_secure = True
            
            # assess what should be the state of this node based on states of all of its dependencies
            old_state = impacted_graph.nodes()[edges[1]]['state']
            new_state = old_state

            # asses state of asset only if it is not already in High-Critical state
            if old_state != 'High-Critical':
            # if atleast one edge from each type of dependency is Medium-Critical or High-Critical, then set new_state = Critial
                check_for_critical = all(deptype in list(num_of_each_dep_of_impacted_asset_st.keys()) for deptype in num_of_each_dep_of_impacted_asset)
                if not atleaset_one_edge_secure:
                    new_state = 'High-Critical'
                elif check_for_critical:
                    new_state = 'Medium-Critical'

                for dep_type, num in num_of_each_dep_of_impacted_asset.items():
                    if dep_type in num_of_each_dep_of_impacted_asset_st.keys():
                        count_lowcritical_per_dep_type = list(num_of_each_dep_of_impacted_asset_st[dep_type]).count('Low-Critical')
                        count_mediumcritical_per_dep_type = list(num_of_each_dep_of_impacted_asset_st[dep_type]).count('Medium-Critical')
                        count_highcritical_per_dep_type = list(num_of_each_dep_of_impacted_asset_st[dep_type]).count('High-Critical')
                        if new_state != 'High-Critical':
                            if (50 > (count_lowcritical_per_dep_type/num)*100 > 0) or (50 > (count_mediumcritical_per_dep_type/num)*100 > 0) or (50 > (count_highcritical_per_dep_type/num)*100 > 0):
                                new_state = 'Low-Critical'     
                            if (100 >= (count_lowcritical_per_dep_type/num)*100 >= 50) or (100 >= (count_mediumcritical_per_dep_type/num)*100 >= 50) or (100 > (count_highcritical_per_dep_type/num)*100 >= 50): 
                                new_state = 'Medium-Critical'     
                            if ((count_highcritical_per_dep_type/num)*100 == 100 ):
                                new_state = 'High-Critical'
                    
                if new_state != old_state:
                    impacted_graph.nodes()[edges[1]]['state'] = new_state
                    next_impacted_assets.append(edges[1])
                    impacted_asset_in_queue[edges[1]] = new_state
                    all_impacted_assets[edges[1]] = {'state' : new_state, 'cascading level' : cascading_level}

    # put a "None" in th ebeginning of the dict
    updated_cascading_level_details = {'None':[]}
    updated_cascading_level_details.update(cascading_level_details)
    #return all_impacted_assets, all_impacted_edges
    return updated_cascading_level_details  
