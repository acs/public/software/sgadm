# Smart Grid Assets and Dependencies Model

This tool displays smart grid assets information and dependencies using a graph-based representation. 

# Instructions for running SGADM tool
- Step 1 : Install Python (version 3.10)
    - Download Python instaler suitable for your OS from : https://www.python.org/downloads/
    - Run the downloaded installer and select the checkbox "_Add Python to PATH_"
- Step 2 : Install required packages:
    - Execute following command on Powershell or Linux/Mac Default Terminal: <br/>
    `pip install numpy pandas networkx pyvis streamlit openpyxl`
- Step 3 : To ensure streamlit is working, type following command on Powershell or Linux/Mac Default Terminal :
    `streamlit hello`
    - It should open streamlit in local browser
    - In case streamlit is blocked by firewall - "Windows Defender Firewal" blocked App - "Allow Access"
- Step 4 : Navigate to the "_static_" folder of streamlit installed package and move .css and .js files of pyvis into this "_static_" folder
    - Look for _static_ folder under installed streamlit package, examples:
        - In Linux/MacOS --> _/usr/local/lib/python3.10/site-packages/streamlit/static_
        - In Windows if Anaconda is used for package management --> _C:\ProgramData\Anaconda3\Lib\site-packages\streamlit\static_ <br/>
        - In Windows, without Anaconda --> _C:\Users\\<user_name>\AppData\Local\Programs\Python\Python310\Lib\site-packages\streamlit\static_ <br/>
    - Download and copy .css and .js files from pyvis into _static_ folder. This is to ensure, the tool runs without network connectivity. On Powershell or Linux/Mac Default Terminal, change current directory to _static_ folder and execute following commands:<br/>
    `curl https://cdnjs.cloudflare.com/ajax/libs/vis/4.16.1/vis-network.min.js -o vis-network.min.js`<br/>
    `curl https://cdnjs.cloudflare.com/ajax/libs/vis/4.16.1/vis.css -o vis.css`
- Step 5 : Download SGADM repository and unzip the folder. On Powershell or Linux/Mac Default Terminal, change current directory to sgadm unzipped folder.
- Step 6 : To run the tool, execute the following command on Powershell or Linux/Mac Default Terminal: <br/>
    `streamlit run ./cyberseas_sgadm.py --browser.gatherUsageStats false`

Note: The example asset and dependencies input-excel ("_asset_dep_example.xlsx_") for the tool, user manual of the tool and other related documents can be found under: https://git.rwth-aachen.de/acs/public/deliverables/cyberseas/sgadm-example-data . Please maintain the format of the input-excel sheet to provide assets and dependencies information. 
